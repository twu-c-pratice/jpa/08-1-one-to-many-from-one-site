package com.twuc.webApp.domain.oneToMany.withJoinColumn;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

// TODO
//
// ParentEntity 应当具备一个自动生成的 id 以及一个字符串 name。除此之外 ParentEntity 应当包含一个
// List<ChildEntity> 类型的 children 字段以显示 parent 和 child 是一对多的关系。请实现 ParentEntity。
// ParentEntity 的参考数据表定义如下：
//
// parent_entity
// +─────────+──────────────+──────────────────────────────+
// | Column  | Type         | Additional                   |
// +─────────+──────────────+──────────────────────────────+
// | id      | bigint       | primary key, auto_increment  |
// | name    | varchar(20)  | not null                     |
// +─────────+──────────────+──────────────────────────────+
//
// <--start-
@Entity
public class ParentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 20, nullable = false)
    private String name;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = false, mappedBy = "parentEntity")
    private List<ChildEntity> children = new ArrayList<>();

    public ParentEntity() {}

    public ParentEntity(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public List<ChildEntity> getChildren() {
        return children;
    }

    public void setChildren(List<ChildEntity> children) {
        this.children = children;
    }
}
